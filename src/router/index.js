import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello/Hello'
import Home from '@/components/home/home'
import Mapstate from '@/components/mapstate/mapstate'
import ui from '@/components/ui/ui'
import addui from '@/components/addui/addui'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/mapstate',
      name: 'Mapstate',
      component: Mapstate
    },
    {
      path: '/ui',
      name: 'ui',
      component: ui
    },
    {
      path: '/addui',
      name: 'addui',
      component: addui
    }
  ]
})
