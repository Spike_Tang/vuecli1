import Vue from 'vue'

Vue.component('m-clipboard', {
  template: '<textarea class="p-clipboard"></textarea>',
  data () {
    return {
    }
  },
  methods: {
    copy (text) {
      var textDom = this.$el
      textDom.value = text
      textDom.select()
      document.execCommand('Copy')

      var message = new Notification('hello world！', {
        body: '你要的代码已经复制到粘贴板',
        icon: '/static/img/1.jpg'
      })
      console.log(message)
    }
  },
  created () {
    Notification.requestPermission().then(function (ok) {
      console.log(ok, 'ok')
    }, function (no) {
      console.log(no, 'no')
    })
  }
})
