// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import VueHighlightJS from 'vue-highlight.js'
import VueAjax from 'vue-resource'

Vue.config.productionTip = false

// Styles 变色样式
import 'highlight.js/styles/monokai-Sublime.css'
// 公用vue 小组件
import './components/common/common.js'

Vue.use(Vuex)
Vue.use(VueHighlightJS)
Vue.use(VueAjax)

const store = new Vuex.Store({
  state: {
    count: 0,
    todos: [
      { id: 1, text: '...', done: true },
      { id: 2, text: '...', done: false }
    ]
  },
  mutations: {
    increment (state, obj) {
      console.log(obj && obj.num)
      obj = obj && obj.num ? obj : {'num': 1}
      state.count += obj.num
    },
    decrement: state => state.count--
  },
  getters: {
    doneTodos: state => {
      return state.todos.filter(todo => todo.done)
    }
  },
  actions: {
    increment (context) {
      context.commit('increment')
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
