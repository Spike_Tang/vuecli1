export default {
  menus: [
    'head',  // 标题
    'bold',  // 粗体
    'italic',  // 斜体
    'strikeThrough',  // 删除线
    'link',  // 插入链接
    'list',  // 列表
    'quote',  // 引用
    'table',  // 表格
    'code',  // 插入代码
    'undo',  // 撤销
    'redo'  // 重复
  ],
  zIndex: 5
}
